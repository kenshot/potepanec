# frozen_string_literal: true

source 'https://rubygems.org'

ruby '~> 2.5.1'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

gem 'aws-sdk', '~> 2.3'
gem 'bootsnap', require: false
gem 'coffee-rails', '~> 4.2'
gem 'jbuilder', '~> 2.5'
gem 'mysql2', '~> 0.5.2'
gem 'paperclip'
gem 'puma', '~> 3.7'
gem 'rails', '~> 5.2.1'
gem 'sass-rails', '~> 5.0'
gem 'solidus', '~> 2.9.0'
gem 'solidus_auth_devise'
gem 'solidus_i18n', github: 'solidusio-contrib/solidus_i18n', branch: 'master'
gem 'uglifier', '>= 1.3.0'
gem 'dotenv-rails'

group :development, :test do
  gem 'annotate'
  gem 'byebug', platforms: %i(mri mingw x64_mingw)
  gem 'factory_bot_rails'
  gem 'pry-byebug'
  gem 'pry-doc'
  gem 'pry-rails'
  gem 'rails-controller-testing'
  gem 'rails-erd'
  gem 'rspec-rails'
  gem 'spring-commands-rspec'
  gem 'rubocop-airbnb', '~> 2'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
  gem 'bullet'
end

group :test do
  gem 'capybara'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i(mingw mswin x64_mingw jruby)
