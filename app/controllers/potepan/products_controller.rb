# frozen_string_literal: true

class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.
      includes(master: [:default_price, :images]).
      limit(MAX_NUMBER_OF_DISPLAY)
  end
end
