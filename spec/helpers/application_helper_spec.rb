require 'rails_helper'

RSpec.describe 'Helpers', type: :helpers do
  describe '#full_title' do
    include ApplicationHelper
    subject { full_title(page_title) }

    context 'when given page_title is Test Title' do
      let(:page_title) { 'Test Title' }

      it { is_expected.to eq 'Test Title - BIGBAG Store' }
    end

    context 'when given page_title is empty string' do
      let(:page_title) { '' }

      it { is_expected.to eq 'BIGBAG Store' }
    end

    context 'when given page_title is nil' do
      let(:page_title) { nil }

      it { is_expected.to eq 'BIGBAG Store' }
    end
  end
end
