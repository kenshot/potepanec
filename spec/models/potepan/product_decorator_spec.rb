# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  let(:taxon)               { create(:taxon) }
  let(:product)             { create(:product, taxons: [taxon]) }
  let!(:related_products)   { create_list(:product, 5, taxons: [taxon]) }
  let!(:no_related_product) { create(:product, name: 'no_related_product') }

  it "related_productsで関連商品を取得すること" do
    expect(product.related_products).to match_array(related_products)
  end

  it "関連商品に選択中のメイン商品は表示されないこと" do
    expect(product.related_products).not_to include product
  end

  it "関連しない商品は表示されないこと" do
    expect(product.related_products).not_to include no_related_product.name
  end
end
