require 'rails_helper'

RSpec.describe Potepan::SampleController, type: :controller do
  describe 'top_page' do
    it "returns top_page" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end
end
