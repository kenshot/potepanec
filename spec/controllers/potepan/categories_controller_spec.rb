require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  let!(:taxonomy) do
    create(:taxonomy, name: 'TestTaxonomy')
  end

  let!(:taxon) do
    create(:taxon, name: 'TestTaxon', taxonomy: taxonomy)
  end

  let!(:product) do
    create(:product, name: 'TestProduct', taxons: [taxon])
  end

  describe 'GET #show' do
    before do
      get :show, params: { id: taxon.id }
    end

    it '200 success' do
      expect(response).to have_http_status 200
    end

    it 'renders the :show template' do
      expect(response).to render_template :show
    end

    it 'assigns the requested article to @taxonomies @taxon @products' do
      expect(assigns(:taxonomies).first).to eq taxonomy
      expect(assigns(:taxon)).to eq taxon
      expect(assigns(:products)).to eq taxon.products
    end
  end
end
