# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET show' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, name: 'Test Coat', taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    it 'returns successfully' do
      expect(response).to be_successful
    end

    it 'rendering to show' do
      expect(response).to render_template :show
    end

    it 'requested product' do
      expect(assigns(:product)).to eq product
    end

    it '４件の関連商品が取得できていること' do
      expect(assigns(:related_products).count).to eq 4
    end
  end
end
