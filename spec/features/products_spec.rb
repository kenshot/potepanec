# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Products', type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon)    { create(:taxon, taxonomy: taxonomy) }
  let(:product)  { create(:product, taxons: [taxon]) }
  let!(:no_related_product) { create(:product, name: 'no_related_product') }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  describe 'products page' do
    before do
      visit potepan_product_path(product.id)
    end

    it 'products page responce' do
      within '.media-body' do
        expect(page).to have_content product.name
        expect(page).to have_content product.display_price
        expect(page).to have_content product.description
      end
    end

    it 'link to home' do
      click_link 'HOME', match: :prefer_exact
      expect(current_path).to eq potepan_index_path
    end

    it 'link to categories page' do
      expect(page).to have_link '一覧ページへ戻る'
      click_link '一覧ページへ戻る', match: :prefer_exact
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    it "関連商品が４件表示される" do
      within '.productsContent' do
        expect(page).to have_content related_products[0].name
        expect(page).to have_content related_products[1].name
        expect(page).to have_content related_products[2].name
        expect(page).to have_content related_products[3].name
      end
    end

    it '選択中のメイン商品を関連商品欄に表示しないこと' do
      within '.productsContent' do
        expect(page).not_to have_content no_related_product.name
      end
    end

    it "関連商品をクリックで商品詳細ページへ切り替わる" do
      within '.productsContent' do
        click_on related_products[0].name
        expect(current_path).to eq potepan_product_path(related_products[0].id)
      end
    end
  end
end
