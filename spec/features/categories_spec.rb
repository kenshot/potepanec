require 'rails_helper'

RSpec.feature 'Categories', type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon)    { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:diff_product) { create(:product) }

  describe 'categories page' do
    before do
      visit potepan_category_path(taxon.id)
    end

    it 'categories page responce' do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content taxon.name
    end

    it 'product info is there' do
      expect(page).to have_content product.name
      expect(page).not_to have_content diff_product.name
    end

    it 'link to product detail page' do
      click_link taxon.name
      expect(page).to have_content product.name
      click_link product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end

    it 'link to home' do
      click_link 'HOME', match: :prefer_exact
      expect(current_path).to eq potepan_index_path
    end
  end
end
